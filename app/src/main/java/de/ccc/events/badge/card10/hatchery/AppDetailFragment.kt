/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.hatchery

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.common.LoadingDialog
import de.ccc.events.badge.card10.filetransfer.TransferJob
import kotlinx.android.synthetic.main.app_detail_fragment.*
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import java.io.File

private const val TAG = "AppDetailFragment"

class AppDetailFragment : Fragment() {

    private lateinit var app: App

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app = AppDetailFragmentArgs.fromBundle(requireArguments()).app

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.app_detail_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        label_name.text = app.name
        label_download_count.text = getString(R.string.app_detail_downloads, app.download_counter)
        label_content_size.text = getString(R.string.app_detail_content_size, app.size_of_content)

        label_description.text = app.description

        button_download.setOnClickListener {
            val ctx = activity ?: throw java.lang.IllegalStateException()

            val loadingDialog = LoadingDialog()
            loadingDialog.show(requireFragmentManager(), "loading")

            val errorDialog =
                AlertDialog.Builder(ctx).setMessage(R.string.hatchery_error_generic)
                    .setPositiveButton(
                        R.string.dialog_action_ok
                    ) { dialog, _ -> dialog.dismiss() }
                    .create()

            val nav = findNavController();
            ReleaseDownload(app, ctx.cacheDir, loadingDialog, errorDialog, nav).execute()
        }
    }

    private class ReleaseDownload(
        private val app: App,
        private val cacheDir: File,
        private val loadingDialog: LoadingDialog,
        private val errorDialog: AlertDialog,
        private val navController: NavController
    ) : AsyncTask<Void, Void, List<TransferJob>?>() {

        override fun doInBackground(vararg p0: Void?): List<TransferJob>? {
            return try {
                cacheDir.deleteRecursively()
                cacheDir.mkdir()
                val appDir = File(cacheDir.absolutePath + "/apps").mkdirs()

                val inputStream = HatcheryClient().openDownloadStream(app)
                val file = File.createTempFile(app.slug, ".tar.gz", cacheDir)
                val outputStream = file.outputStream()

                inputStream.copyTo(outputStream)

                val appFiles = mutableListOf<TransferJob>()
                val tarStream = TarArchiveInputStream(GzipCompressorInputStream(file.inputStream()))
                while (true) {
                    val entry = tarStream.nextTarEntry ?: break
                    if (entry.isDirectory) {
                        continue
                    }

                    // TODO: A bit hacky. Maybe there is a better way?
                    val targetFile = File(cacheDir, "apps/${entry.name}")
                    targetFile.parentFile?.mkdirs()
                    targetFile.createNewFile()
                    Log.d(TAG, "Extracting ${entry.name} to ${targetFile.absolutePath}")
                    tarStream.copyTo(targetFile.outputStream())
                    appFiles.add(TransferJob(targetFile.toUri(), "/apps/${entry.name}"))
                }

                val launcher = createLauncher(app.slug, cacheDir)
                appFiles.add(launcher)

                appFiles
            } catch (e: Exception) {
                null
            }
        }

        override fun onPostExecute(jobs: List<TransferJob>?) {
            if (jobs == null) {
                loadingDialog.dismiss()
                errorDialog.show()
                return
            }

            loadingDialog.dismiss()
            val nav = AppDetailFragmentDirections.startBatchTransfer(jobs.toTypedArray())
            navController.navigate(nav)

        }

        fun createLauncher(slug: String, cacheDir: File): TransferJob {
            val fileName = "$slug.py"
            val file = File(cacheDir, fileName)
            file.createNewFile()

            val src = """
                # Launcher script for $slug
                import os
                os.exec("/apps/$slug/__init__.py")
            """.trimIndent()

            file.writeText(src)

            return TransferJob(file.toUri(), "/$fileName")
        }
    }
}
