/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.hatchery

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.common.LoadingDialog
import kotlinx.android.synthetic.main.app_list_fragment.*
import java.lang.ref.WeakReference

class AppListFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var listAdapter: AppListAdapter
    private lateinit var layoutManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.app_list_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        listAdapter = AppListAdapter { app: App -> onAppClicked(app) }

        layoutManager = LinearLayoutManager(activity)

        recyclerView = list
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = listAdapter

        val loadingDialog = LoadingDialog()
        loadingDialog.show(requireFragmentManager(), "loading")
        DownloadTask(activity as Context, listAdapter, loadingDialog).execute()
    }

    private fun onAppClicked(app: App) {
        findNavController().navigate(AppListFragmentDirections.actionAppListFragmentToAppDetailFragment(app))
    }

    private class DownloadTask(
        context: Context,
        private val adapter: AppListAdapter,
        private val loadingDialog: LoadingDialog
    ) : AsyncTask<Void, Void, List<App>?>() {

        private val context = WeakReference<Context>(context)

        override fun doInBackground(vararg p0: Void?): List<App>? {
            val client = HatcheryClient()

            return try {
                client.getAppList()
            } catch (e: HatcheryClientException) {
                null
            }
        }

        override fun onPostExecute(result: List<App>?) {
            if (result == null) {
                loadingDialog.dismiss()
                val ctx = context.get() ?: throw IllegalStateException()
                AlertDialog.Builder(ctx).setMessage(R.string.hatchery_error_generic)
                return
            }

            adapter.update(result)
            adapter.notifyDataSetChanged()
            loadingDialog.dismiss()
        }
    }
}
