/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.main

import android.bluetooth.BluetoothAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import de.ccc.events.badge.card10.CARD10_BLUETOOTH_MAC_PREFIX
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.common.ConnectionService
import de.ccc.events.badge.card10.common.GattListener
import de.ccc.events.badge.card10.time.TimeUpdateDialog
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment(), GattListener {
    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(activity!!.intent.action == "application/x.card10.app") {
            findNavController().navigate(MainFragmentDirections.Installer())
            return null
        }
        return inflater.inflate(R.layout.main_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val con = findNavController()
        button_pair.setOnClickListener { con.navigate(MainFragmentDirections.Scan()) }
        button_mood.setOnClickListener { con.navigate(MainFragmentDirections.Mood()) }
        button_beautiful.setOnClickListener { con.navigate(MainFragmentDirections.Beautiful()) }
        button_hatchery.setOnClickListener { con.navigate(MainFragmentDirections.AppList()) }
        button_send.setOnClickListener { con.navigate(MainFragmentDirections.Transfer()) }

        button_set_time.setOnClickListener {
            val dialogFragment = TimeUpdateDialog()
            dialogFragment.show(fragmentManager!!, "time")
            dialogFragment.setTime()
            dialogFragment.dismiss()
        }

        ConnectionService.addGattListener("main", this)

        val bondedCard10s =
            bluetoothAdapter.bondedDevices.filter { it.address.startsWith(CARD10_BLUETOOTH_MAC_PREFIX, true) }

        if (bondedCard10s.isNotEmpty()) {
            val ctx = activity ?: throw IllegalStateException()
            ConnectionService.connect(ctx)
            showConnectingView()
        } else {
            label_status.text = getString(R.string.main_label_not_connected)
            showDisconnectedView()
        }
    }


    private fun showConnectedView() {
        // The callback can happen when our UI is not visible
        if (container_connected == null) {
            return
        }

        activity?.runOnUiThread {
            container_connected.visibility = View.VISIBLE
            container_disconnected.visibility = View.GONE
            button_pair.text = getString(R.string.main_button_manage_pairings)

            button_hatchery.isEnabled = true
            button_send.isEnabled = true
            button_mood.isEnabled = true
            button_beautiful.isEnabled = true
            button_set_time.isEnabled = true

            val device = ConnectionService.device
            label_status.text = getString(R.string.main_label_connected, device?.name, device?.address)
        }
    }

    private fun showConnectingView() {
        val device = ConnectionService.device
        label_status.text = getString(R.string.main_label_connecting, device?.name, device?.address)
        button_pair.text = getString(R.string.main_button_manage_pairings)
    }

    private fun showDisconnectedView() {
        container_connected.visibility = View.GONE
        container_disconnected.visibility = View.VISIBLE

        button_pair.text = getString(R.string.main_button_pair)
    }

    override fun onConnectionReady() {
        showConnectedView()
    }
}
